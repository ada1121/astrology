//
//  MenuCell.swift
//  PortTrucker
//
//  Created by Admin on 2019/8/29.
//  Copyright © 2019 LiMing. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    
    @IBOutlet weak var imvIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var divider: UIView!
    
    var entity : MenuModel!{
        didSet {
            imvIcon.image = UIImage(named: entity.icon)
            lblTitle.text = entity.title
            //divider.isHidden = true
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
