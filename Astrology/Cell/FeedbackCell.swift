//
//  FeedbackCell.swift
//  Astrology
//
//  Created by Ubuntu on 11/20/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit
import Kingfisher

class FeedbackCell: UITableViewCell {
    
    
    
    @IBOutlet weak var imvAvatar: UIImageView!
    @IBOutlet weak var feedbackDay: UILabel!
    @IBOutlet weak var feedbackName: UILabel!
    @IBOutlet weak var feedbackContent: UILabel!
    
    var entity : FeedbackModel!{
        
           didSet {
            let url = URL(string: entity.userphoto)
            imvAvatar.kf.setImage(with: url,placeholder: UIImage(named: "useravatar"))
            feedbackDay.text = entity.feedbackday
            feedbackName.text = entity.username
            feedbackContent.text = entity.feedcontent
               
           }
       }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
