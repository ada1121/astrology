//
//  Splash.swift
//  Astrology
//
//  Created by PSJ on 10/30/19.
//  Copyright © 2019 PSJ. All rights reserved.
//


import UIKit
import SwiftyUserDefaults

class Splash: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        gotoHome()

    }
    
    func gotoHome() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            
            if Defaults[.email] == nil {
               loginNav!.modalPresentationStyle = .fullScreen
                self.present(loginNav, animated: true, completion: nil)
            }
            else {
//                navigationController2.modalPresentationStyle = .fullScreen
//                self.present(navigationController2, animated: true, completion: nil)
                print("no UiviewController")
            }
           })
       }
    
}

