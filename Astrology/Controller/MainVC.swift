//
//  MainVC.swift
//  Astrology
//
//  Created by Admin on 11/9/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit

class MainVC: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        creatNav()
        openMenu()
        navBarTransparent()
        navBarHidden()
    }
    
}
