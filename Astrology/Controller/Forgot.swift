//
//  Forgot.swift
//  Loyalty
//
//  Created by PSJ on 10/12/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import Kingfisher
import SwiftyUserDefaults

class Forgot : BaseVC1, ValidationDelegate, UITextFieldDelegate {
    @IBOutlet weak var edtEmail: UITextField!
    
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var pinlabel: UILabel!
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        edtEmail.addPadding(.left(30))
        //profile.image = GlobalUser.photo
        if Defaults[.profile] != nil {
            
        let imageURL = Defaults[.profile]
        let url = URL(string:imageURL!)
        profile.kf.setImage(with: url, placeholder: UIImage(named: "avatar-1"))
            pinlabel.isHidden = true
            
        }
        
        else{
            pinlabel.isHidden = true
            print("first input")
        }
        
    }
    @IBAction func submitBtnclicked(_ sender: Any) {
               validator.registerField( edtEmail, errorLabel: nil , rules: [ RequiredRule(),EmailRule()])
               validator.styleTransformers(success:{ (validationRule) -> Void in
                   
                   // clear error label
                   validationRule.errorLabel?.isHidden = true
                   validationRule.errorLabel?.text = ""
                   
                   if let textField = validationRule.field as? UITextField {
                       textField.layer.borderColor = UIColor.green.cgColor
                       textField.layer.borderWidth = 1
                   } else if let textField = validationRule.field as? UITextView {
                       textField.layer.borderColor = UIColor.green.cgColor
                       textField.layer.borderWidth = 1
                   }
               }, error:{ (validationError) -> Void in
                   print("error")
                   validationError.errorLabel?.isHidden = false
                   validationError.errorLabel?.text = validationError.errorMessage
                   if let textField = validationError.field as? UITextField {
                       textField.layer.borderColor = UIColor.red.cgColor
                       textField.layer.borderWidth = 1.0
                   } else if let textField = validationError.field as? UITextView {
                       textField.layer.borderColor = UIColor.red.cgColor
                       textField.layer.borderWidth = 1.0
                   }
               })
               validator.validate(self)
    }
    
    func validationSuccessful() {
        forgotapi(useremail: edtEmail.text!)
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        showMessage1("Please Input Correct!")
    }
    
    func forgotapi(useremail : String){
        showHUD1()
        ApiManager.forgot(email: useremail) { (isSuccess, data) in
            self.hideHUD1()
            if isSuccess{
                //self.showMessage1(data as! String)
                self.pinlabel.isHidden = false
                self.pinlabel.text = (data as! String)
                
            }
            else{
                if data == nil{
                    self.showMessage1("Connection Fail")
                }
                else{
                    let result_message = data as! String
                    if(result_message == "1"){
                        self.showMessage1("Non Exist Email")
                    }
                    
                }
                
            }
            
        }
        
    }
}
