//
//  Singup.swift
//  Loyalty
//
//  Created by PSJ on 10/12/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox

class Signup: BaseVC1, ValidationDelegate, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var edtName: UITextField!
    @IBOutlet weak var edtEmail: UITextField!
    @IBOutlet weak var edtPwd: UITextField!
    @IBOutlet weak var edtConPwd: UITextField!
    @IBOutlet weak var shadow: UIView!
    @IBOutlet weak var checkBox: GDCheckbox!
    
    let validator = Validator()
    
   
    var pictureData = [CapturePicModel]()
    var imageFils = [String]()
    var imagePicker: ImagePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        EdtInit()
        profileImg.addTapGesture(tapNumber: 1, target: self, action: #selector(onEdtPhoto))
        shadow.ShadowCenter()
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)

    }
    
    ///  - Description: edit text initialize
     func EdtInit()  {
        edtName.addPadding1(.left(25))
        edtEmail.addPadding1(.left(25))
        edtPwd.addPadding1(.left(25))
        edtConPwd.addPadding1(.left(25))
    }
    
    @IBAction func gotoTerms(_ sender: Any) {
        gotoNavPresent1("Terms")
    }
    
    @IBAction func signUpBtnclicked(_ sender: Any) {
        
        validator.registerField(edtName, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(edtEmail, errorLabel: nil , rules: [RequiredRule(),EmailRule()])
        validator.registerField(edtPwd, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule()])
        validator.registerField(edtConPwd, errorLabel: nil , rules: [RequiredRule(),ConfirmationRule(confirmField: edtPwd)])
        validator.styleTransformers(success:{ (validationRule) -> Void in
            
            // clear error label
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
            
            if let textField = validationRule.field as? UITextField {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 1
            } else if let textField = validationRule.field as? UITextView {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 1
            }
        }, error:{ (validationError) -> Void in
            print("error")
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            } else if let textField = validationError.field as? UITextView {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            }
        })
        validator.validate(self)
    }
    
    func validationSuccessful() {
        
        if checkBox.isOn == false{
            showMessage1("Please Agree Terms of Use!")
        }
        
        else {
              signupApi(usertname : edtName.text!, email : edtEmail.text!, password : edtPwd.text!, images : imageFils)
            
        }
        
        
       
    }
    
    func signupApi( usertname : String, email : String, password : String , images : [String]){
        showHUD1()
        ApiManager.signup(username : usertname, email: email, password : password , images : images ) { (isSuccess, data) in
            self.hideHUD1()
            if isSuccess{
                let dict = JSON(data as Any)
                self.showMessage1("Register Success!")
                print (dict)
                self.loginApi(useremail: email, pwd: password)
                
            }
            else{
                if data == nil{
                    self.showMessage1("Connection Error!")
                }
                else{
                    let result_message = data as! String
                    if(result_message == "1"){
                        self.showMessage1("Email Already Exist!")
                        
                    }
                    else{
                        self.showMessage1("Please Select Photo File")
                    }
                }
                
            }
            
        }
        
    }
    
    func loginApi(useremail : String, pwd : String){
        showHUD1()
        ApiManager.login(useremail: useremail, password: pwd) { (isSuccess, data) in
            self.hideHUD1()
            if isSuccess{
                let dict = JSON(data as Any)
                GlobalUser = UserModel(dict)
                print(GlobalUser.photo)
                
                Defaults[.email] = useremail
                Defaults[.id] = GlobalUser.user_id
                Defaults[.profile] = GlobalUser.photo
                Defaults[.username] = GlobalUser.username
                
                self.signupsuccess()
                
            }
            else{
                if data == nil{
                    self.showMessage1("Connection Fail")
                }
                else{
                    let result_message = data as! String
                    if(result_message == "1"){
                        self.showMessage1("Non Exist Email")
                    }
                    else{
                        self.showMessage1("Password Incorrect")
                    }
                }
                
            }
            
        }
        
    }
    
    func signupsuccess(){
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: "MainNav")
        toVC!.modalPresentationStyle = .fullScreen
        self.present(toVC!, animated: true, completion: nil)
        
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        print("validation error")
    }
    
    @objc func onEdtPhoto(gesture: UITapGestureRecognizer) -> Void {
           self.imagePicker.present(from: profileImg)
    }
    
    func gotoUploadProfile() {
           for one in pictureData {
               let image = one.imgFile!
               imageFils.append(saveToFile(image:image,filePath:"photo",fileName:randomString(length: 2))) // Image save action
           }
        profileImg.image = pictureData.last?.imgFile
        
    }
    
}

extension Signup: ImagePickerDelegate { // custom uiImagePicker
    
    func didSelect(image: UIImage?) {
        self.pictureData.removeAll()
        if let image = image {
        let one = CapturePicModel(image) // data add
        pictureData.append(one) // append action
        self.gotoUploadProfile()
            
        }
        else{
            print("No picture")
        }
        
    }
}

