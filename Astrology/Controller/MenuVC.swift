//
//  MenuVC.swift
//  PortTrucker
//
//  Created by LiMing on 8/13/19.
//  Copyright © 2019 LiMing. All rights reserved.
//

import Foundation
import UIKit

class MenuVC : BaseVC, UITableViewDelegate, UITableViewDataSource{
   
    
    @IBOutlet weak var spaceView: UIView!
    @IBOutlet weak var tbvMenu: UITableView!
    
    var dataSource =  [MenuModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
        spaceView.addTapGesture(tapNumber: 1, target: self, action: #selector(onSpace))
    }
    
    func loadData() {
        let menuIcons = ["zodiac_menu", "numerology_menu", "palmstory_menu", "feedback_menu", "candle_menu", "ball_menu", "constellation_menu", "natalchart_menu", "feedback_menu", "socialmedia_menu", "element_menu", "socialmedia_menu", "feedback_menu","feedback_menu"]
        let menuText = ["Zodiac", "Numerology", "Palmstry", "Tarot card Reading", "Candlelight", "MagicBall", "Constellations", "Natal Chart", "Mood Guide", "Dream Interpretation", "Elements", "Social Media", "Feedback","Logout"]
        
        for i in 0 ..< menuIcons.count {
            let one = MenuModel.init(icon: menuIcons[i], title: menuText[i])
            dataSource.append(one)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @objc func onSpace(gesture: UITapGestureRecognizer) -> Void {
          closeMenu()
        openStatue = false
        exceptionStatue = false
        print(openStatue )
         print(openStatue )
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbvMenu.dequeueReusableCell(withIdentifier: "MenuCell", for:indexPath) as! MenuCell
        cell.entity = dataSource[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.init(named: "coloredtText")
        let row = indexPath.row
        
        if row != 13 && row != 12 {
        gotoNavPresent(storyName[row])
        
            
        }
        if row == 13{
            
            let loginVC = self.storyboard?.instantiateViewController( withIdentifier: "Login")
              
            loginNav = UINavigationController(rootViewController: loginVC!)
              
            loginNav.modalPresentationStyle = .fullScreen
             self.present(loginNav,animated: true,completion: nil)
             closeMenu()
        }
        else {
            print("error")
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.init(named: "colorPrimary")
    }
}
