//
//  Terms.swift
//  Loyaltiyapp
//
//  Created by PSJ on 11/6/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit

class Terms: UIViewController {

     @IBOutlet weak var Terms_txt: UITextView!
       
       override func viewDidLoad() {
           super.viewDidLoad()
           if let filepath = Bundle.main.path(forResource: "about", ofType: "txt") {
               do {
                   let contents = try String(contentsOfFile: filepath)
                   Terms_txt.text = contents
                  // print(contents)
               } catch {
                   // contents could not be loaded
               }
           } else {
               // example.txt not found!
           }
       }

}
