//
//  FeedbackVC.swift
//  Astrology
//
//  Created by Ubuntu on 11/18/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit

class FeedbackVC: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tbvFeedback: UITableView!
    var dataSource =  [FeedbackModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }

  func loadData() {
                let feedbackdayData = ["1.12", "2.23", "3.12", "5.22", "8.15" ]
                let feedbackContent = ["Zodiac", "Numerology", "Palmstry", "Tarot card Reading", "Candlelight"]
    
                let feedbackUsername = ["John", "Moli", "Teddy", "Rosy", "Hellin"]
                let photofakeData = ["https://res.cloudinary.com/demo/image/upload/sample.jpg", "https://res.cloudinary.com/demo/image/upload/sample.jpg", "https://res.cloudinary.com/demo/image/upload/sample.jpg", "https://res.cloudinary.com/demo/image/upload/sample.jpg", "https://res.cloudinary.com/demo/image/upload/sample.jpg"]
                
                for i in 0 ..< feedbackdayData.count {
                    let one = FeedbackModel.init(userphoto : photofakeData[i], feedbackday : feedbackdayData[i], username : feedbackUsername[i], feedbackcontent : feedbackContent[i])
                    dataSource.append(one)
                }
            }
            
            func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return dataSource.count
            }
            
            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
                let cell = tbvFeedback.dequeueReusableCell(withIdentifier: "FeedbackCell", for:indexPath) as! FeedbackCell
                cell.entity = dataSource[indexPath.row]
                
                return cell
            }
            
            func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//               tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.init(named: "coloredtText")
                let row = indexPath.row
                
            }
            
            func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
                tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.init(named: "colorPrimary")
            }
        }
