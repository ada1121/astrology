//
//  Login.swift
//  Loyalty
//
//  Created by PSJ on 10/12/19.
//  Copyright © 2019 PSJ. All rights reserved.
//
import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox

class Login: BaseVC1, ValidationDelegate, UITextFieldDelegate  {

    @IBOutlet weak var edtPwd: UITextField!
    @IBOutlet weak var edtEmail: UITextField!
    let validator = Validator()
    
    @IBOutlet weak var checkBox: GDCheckbox!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        edtInit()
        navBarTransparent1()
        loadlayout()
        
    }
    
    func loadlayout() {
        edtPwd.text = "123"
        edtEmail.text = "jerilyn@gmail.com"
    }
    
    @IBAction func gotoTerms(_ sender: Any) {
        print("terms")
        gotoNavPresent1("Terms")
        
    }
    @IBAction func gotoHome(_ sender: Any) {
        // this part must have been changed
        // validate treatment
        validator.registerField(edtEmail, errorLabel: nil , rules: [ RequiredRule(),EmailRule()])
        validator.registerField(edtPwd, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule()])
        validator.styleTransformers(success:{ (validationRule) -> Void in
            
            // clear error label
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
            
            if let textField = validationRule.field as? UITextField {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 1
            } else if let textField = validationRule.field as? UITextView {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 1
            }
        }, error:{ (validationError) -> Void in
            print("error")
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            } else if let textField = validationError.field as? UITextView {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            }
        })
        validator.validate(self)
       
    }
    
    func validationSuccessful() {
        //loginApi(useremail: edtEmail.text!, pwd: edtPwd.text!)
        loginSuccess()
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
           showMessage1("Please Input Correct!")
       }
    
    func loginApi(useremail : String, pwd : String){
           //showHUD1()
           ApiManager.login(useremail: useremail, password: pwd) { (isSuccess, data) in
               //self.hideHUD1()
               if isSuccess{
                   let dict = JSON(data as Any)
                   GlobalUser = UserModel(dict)
                   print(GlobalUser.photo)
                   self.loginSuccess()
                   Defaults[.email] = useremail
                   Defaults[.profile] = GlobalUser.photo
                   Defaults[.username] = GlobalUser.username
                   Defaults[.id] = GlobalUser.user_id
                print("\(Defaults[.id]! as Any)")
               }
               else{
                   if data == nil{
                       self.showMessage1("Connection Fail")
                   }
                   else{
                       let result_message = data as! String
                       if(result_message == "1"){
                           self.showMessage1("Non Exist Email")
                       }
                       else{
                           self.showMessage1("Password Incorrect")
                       }
                   }
                   
               }
               
           }
           
       }
    
    func loginSuccess() {
        
        if checkBox.isOn == false{
           showMessage1("Please Agree Terms of Use!")
        }
        else{
         
//         mainNav!.modalPresentationStyle = .fullScreen
//         self.present(mainNav, animated: true, completion: nil)
            
            let mainVC = self.storyboard?.instantiateViewController( withIdentifier: "MainVC")
              
            mainNav = UINavigationController(rootViewController: mainVC!)
              
            mainNav.modalPresentationStyle = .fullScreen
             self.present(mainNav,animated: true,completion: nil)

        }
        
        //print("here")
    }
    
    @IBAction func gotoSignUp(_ sender: Any) {
        gotoNavPresent1("Signup")
    }
    @IBAction func gotoForgot(_ sender: Any) {
        gotoNavPresent1("Forgot")
    }
    ///  - Description: editText initialize
     func edtInit () {
        edtEmail.addPadding1(.left(25))
        edtPwd.addPadding1(.left(25))
    }
}


