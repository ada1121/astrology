//
//  ApiManager.swift
//  PortTrucker
//
//  Created by PSJ on 9/12/19.
//  Copyright © 2019 LiMing. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON

// ************************************************************************//
                                // Royalty Radio //
// ************************************************************************//
let API = "http://www.royaltyradio.us/api/"

let LOGIN = API + "login"
let SIGNUP = API + "register"
let FORGOT = API + "forgot"
let BLOCK = API + "block"
let REPORT = API + "report"




struct PARAMS {
    
    // ************************************************************************//
                                    // Royalty Radio //
    // ************************************************************************//
    static let STATUS = "result_code"
    static let TRUE = "0"
    static let USERINFO = "user_info"
    static let USERID = "user_id"
    static let EMAIL = "email"
    static let USERNAME = "username"
    static let PHOTO = "photo"
    static let PASSWORD = "password"
    static let TIMESTAMP = "timestamp"
}

class ApiManager {
    
    class func login(useremail : String, password : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let params = ["email" : useremail, "password" : password ] as [String : Any]
        
        Alamofire.request(LOGIN, method:.post, parameters:params)
        .responseJSON { response in
            
            switch response.result {
                
            case .failure:
                completion(false, nil)
            case .success(let data):
                let dict = JSON(data)
                let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                let user_info = dict[PARAMS.USERINFO].object
                let userdata = JSON(user_info)
                if status == PARAMS.TRUE {
                    completion(true, userdata)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func signup( username : String, email : String, password : String, images : [String], completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {

          let requestURL = SIGNUP
                 
                 Alamofire.upload(
                     multipartFormData: { multipartFormData in
                        
                        for one in images { multipartFormData.append(URL(fileURLWithPath:one),withName:PARAMS.PHOTO)
                            }
                        multipartFormData.append( username.data(using:String.Encoding.utf8)!, withName: PARAMS.USERNAME)
                        multipartFormData.append( email.data(using:String.Encoding.utf8)!, withName: PARAMS.EMAIL)
                        multipartFormData.append( password.data(using:String.Encoding.utf8)!, withName: PARAMS.PASSWORD)
                         
                 },
                     to: requestURL,
                     encodingCompletion: { encodingResult in
                         switch encodingResult {
                         case .success(let upload, _, _):
                             upload.responseJSON { response in
                                 switch response.result {
                                 case .failure: completion(false, nil)
                                 case .success(let data):
                                     let dict = JSON(data)
                                     print(dict)
                                     
                                     let status = dict[PARAMS.STATUS].stringValue
                                     if status == PARAMS.TRUE {
                                         completion(true, dict)
                                     } else {
                                         completion(false, status)
                                     }
                                 }
                             }
                         case .failure(let encodingError):
                             print(encodingError)
                             completion(false, nil)
                         }
                 })
    }
    
    class func forgot(email : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
    
            let params = [PARAMS.EMAIL: email] as [String : Any]
    
            Alamofire.request( FORGOT, method:.post, parameters:params)
                .responseJSON { response in
    
                    switch response.result {
    
                    case .failure:
                        completion(false, nil)
                    case .success(let data):
                        let dict = JSON(data)
                        let status = dict[PARAMS.STATUS].stringValue
                        let pincode = dict["pincode"].stringValue
                        if status == PARAMS.TRUE {
                            completion(true, pincode)
                        } else {
                            completion(false, status)
                        }
                    }
            }
        }
    
    class func block( timestamp : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
    
            let params = [PARAMS.TIMESTAMP: timestamp] as [String : Any]
    
            Alamofire.request( BLOCK, method:.post, parameters:params)
                .responseJSON { response in
    
                    switch response.result {
    
                    case .failure:
                        completion(false, nil)
                    case .success(let data):
                        let dict = JSON(data)
                        let status = dict[PARAMS.STATUS].stringValue
                        
                        if status == PARAMS.TRUE {
                            completion(true, status)
                        } else {
                            completion(false, status)
                        }
                    }
            }
        }
    
    class func report( timestamp : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
    
        let params = [PARAMS.TIMESTAMP: timestamp] as [String : Any]
    
            Alamofire.request( REPORT, method:.post, parameters:params)
                .responseJSON { response in
    
                    switch response.result {
    
                    case .failure:
                        completion(false, nil)
                    case .success(let data):
                        let dict = JSON(data)
                        let status = dict[PARAMS.STATUS].stringValue
                        
                        if status == PARAMS.TRUE {
                            completion(true, status)
                        } else {
                            completion(false, status)
                        }
                    }
            }
        }
    
}
