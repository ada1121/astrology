//
//  UserModel.swift
//  Astrology
//
//  Created by Admin on 11/9/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import Foundation
import SwiftyJSON
import Kingfisher
import UIKit

class UserModel {
    var user_id:String = ""
    var email:String = ""
    var username:String = ""
    var photo:String = ""
    init (_ Userdata : JSON)
    {
        self.user_id = Userdata [PARAMS.USERID].stringValue
        self.email=Userdata[PARAMS.EMAIL].stringValue
        self.username = Userdata[PARAMS.USERNAME].stringValue
        self.photo = Userdata[PARAMS.PHOTO].stringValue
    }
}

