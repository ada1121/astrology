//
//  MenuModel.swift
//  Astrology
//
//  Created by Ubuntu on 11/14/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import Foundation

class MenuModel {
    var icon : String
    var title : String
    
    init(icon : String, title : String) {
        self.icon = icon
        self.title = title
    }
}

