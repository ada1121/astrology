//
//  FeedBackModel.swift
//  Astrology
//
//  Created by Ubuntu on 11/21/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import Foundation

class FeedbackModel {
    var userphoto : String
    var feedbackday : String
    var username : String
    var feedcontent : String
    
    
    init(userphoto : String, feedbackday : String, username : String, feedbackcontent : String) {
        self.userphoto = userphoto
        self.feedbackday = feedbackday
        self.username = username
        self.feedcontent = feedbackcontent
    }
}
