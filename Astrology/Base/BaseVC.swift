//
//  BaseVC.swift
//  Astrology
//
//  Created by Admin on 11/9/19.
//  Copyright © 2019 PSJ. All rights reserved.
//


import UIKit
import Toast_Swift
import SVProgressHUD
import SwiftyUserDefaults
import SwiftyJSON

class BaseVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
          
        
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        
        if (sender.direction == .left) {
            
            self.closeMenu()
            openStatue = false
           
        }
        
        if (sender.direction == .right && openStatue == false)  {
            
            self.openMenu()
            openStatue = true
            
            
        }
        else{
            
            self.closeMenu()
            openStatue = false
        }
    }
    
    func creatNav() {
        
        menuVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuVC") as! MenuVC)
        menuVC.view.frame = CGRect(x: -UIScreen.main.bounds.width, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)

        darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
        darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
    
    // slider navigation open and close
    func openMenu(){
        
        darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.view.addSubview(darkVC.view)
        self.addChild(darkVC)
        UIView.animate(withDuration: 0.3, animations: {
            menuVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(menuVC.view)
            self.addChild(menuVC)
        })
    }
    
    func popMenu(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func closeMenu(){
        
        darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        darkVC.view.removeFromSuperview()
        UIView.animate(withDuration: 0.3, animations: {
            menuVC.view.frame = CGRect(x: -UIScreen.main.bounds.width, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            //self.menuVC.view.removeFromSuperview()
            menuVC.view.willRemoveSubview(menuVC.view)
        })
    }
    
    func logout() {
        
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginNav") as! LoginNav
//        nextVC.modalPresentationStyle = .fullScreen
//        self.present(nextVC, animated: true, completion: nil)
    }
    
      //toast message
    func showMessage(_ message : String) {
        self.view.makeToast(message)
    }

    // basic detail action
    func showHUD() {
        SVProgressHUD.show();
    }

    func showHUDWithTitle(title: String) {
        SVProgressHUD.show(withStatus: title)
    }

    func hideHUD() {
        SVProgressHUD.dismiss()
    }
    
    func gotoNavPresent(_ storyname : String) {
           
           let toVC = self.storyboard?.instantiateViewController(withIdentifier: storyname)
           self.navigationController?.pushViewController(toVC!, animated: true)
          
   }
    
    ///  - Description: navigation bar hidden
     func navBarHidden() {
        self.navigationController?.isNavigationBarHidden = true
    }
    ///  - Description: navigation bar transparent
    func navBarTransparent() {
           self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
           self.navigationController?.navigationBar.shadowImage = UIImage()
           self.navigationController?.navigationBar.isTranslucent = true
           self.navigationController?.view.backgroundColor = .clear
       }
    
}

extension UIApplication {
    class var statusBarBackgroundColor: UIColor? {
        get {
            return (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor
        } set {
            (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor = newValue
        }
    }
}

extension UIView {
    
    func visiblity(gone: Bool, dimension: CGFloat = 0.0, attribute: NSLayoutConstraint.Attribute = .height) -> Void {
        if let constraint = (self.constraints.filter{$0.firstAttribute == attribute}.first) {
            constraint.constant = gone ? 0.0 : dimension
            self.layoutIfNeeded()
            self.isHidden = gone
        }
    }

}

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}

extension UITextField {
    
    enum PaddingSide {
        case left(CGFloat)
        case right(CGFloat)
        case both(CGFloat)
    }
    
    func addPadding(_ padding: PaddingSide) {
        
        self.leftViewMode = .always
        self.layer.masksToBounds = true
        
        
        switch padding {
            
        case .left(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.rightViewMode = .always
            
        case .right(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = paddingView
            self.rightViewMode = .always
            
        case .both(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            // left
            self.leftView = paddingView
            self.leftViewMode = .always
            // right
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
}

extension UIView {
    func addTapGesture(tapNumber: Int, target: Any, action: Selector) {
        let tap = UITapGestureRecognizer(target: target, action: action)
        tap.numberOfTapsRequired = tapNumber
        addGestureRecognizer(tap)
        isUserInteractionEnabled = true
    }
    
    func lock() {
        if let _ = viewWithTag(10) {
            //View is already locked
        }
        else {
            let lockView = UIView(frame: bounds)
            lockView.backgroundColor = UIColor(white: 0.0, alpha: 0.75)
            lockView.tag = 10
            lockView.alpha = 0.0
            let activity = UIActivityIndicatorView(style: .white)
            activity.hidesWhenStopped = true
            activity.center = lockView.center
            lockView.addSubview(activity)
            activity.startAnimating()
            addSubview(lockView)
            
            UIView.animate(withDuration: 0.2, animations: {
                lockView.alpha = 1.0
            })
        }
    }
    
    func unlock() {
        if let lockView = viewWithTag(10) {
            UIView.animate(withDuration: 0.2, animations: {
                lockView.alpha = 0.0
            }, completion: { finished in
                lockView.removeFromSuperview()
            })
        }
    }
    
    func fadeOut(_ duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
    
    func fadeIn(_ duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
}

extension UIView {
    func dropShadowleft(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.white.cgColor
        //layer.shadowOpacity = 0.7
        //layer.shadowOffset = .zero
        //layer.shadowOffset = CGSize(width: 10.0, height: 10.0)
        
        layer.shadowOpacity = 0.8
        layer.shadowOffset = CGSize(width: -1.0, height: 1.0)// height increament bottom increament
        layer.shadowRadius = 3.0 // height increament : spread increament
        //layer.shadowColor = UIColor.yellowColor().CGColor
        //layer.backgroundColor = UIColor.grayColor()

        //layer.shadowRadius = 1
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func dropShadowbottom(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        //layer.shadowOpacity = 0.7
        //layer.shadowOffset = .zero
        //layer.shadowOffset = CGSize(width: 10.0, height: 10.0)
        
        layer.shadowOpacity = 0.7
        layer.shadowOffset = CGSize(width: 0.0, height:4.0)
        layer.shadowRadius = 2.0
        //layer.shadowColor = UIColor.yellowColor().CGColor
        //layer.backgroundColor = UIColor.grayColor()
        
        //layer.shadowRadius = 1
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}

extension UIScrollView {
    func hideIndicators() {
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
    }
}

extension DefaultsKeys {
    
    static let id = DefaultsKey<String?>("id")
    static let username = DefaultsKey<String?>("username")
    static let email = DefaultsKey<String?>("email")
    static let profile = DefaultsKey<String?>("profile")
   
}
extension UITextField {
    @IBInspectable var placeholderColor: UIColor {
        get {
            return attributedPlaceholder?.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor ?? .clear
        }
        set {
            guard let attributedPlaceholder = attributedPlaceholder else { return }
            let attributes: [NSAttributedString.Key: UIColor] = [.foregroundColor: newValue]
            self.attributedPlaceholder = NSAttributedString(string: attributedPlaceholder.string, attributes: attributes)
        }
    }

}

